# GeoModel Contacts

In case of questions, you can contact the GeoModel Core Team at:
[geomodel-core-team@cern.ch](mailto:geomodel-core-team@cern.ch)

We also have an active community of developers that can be contacted at:
[geomodel-developers@cern.ch](mailto:geomodel-developers@cern.ch)
